[Current Site](https://ocds.equalcare.coop)

## License

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://ocds.equalcare.coop">Equal Care Co-operative Open Contracting Data</a> by <span property="cc:attributionName">Equal Care Co-operative</span> is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:block;">CC BY 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>

## Installing

```sh
python3 -m venv venv
source /venv/bin/activate
pip install -r requirements.txt
```

Note: the ocdsadditions tool is not version controlled. If there have been any
updates, you'll have to install them by using the `--force` flag with pip.

## Building

Ensure you're in the virtual environment already.

```sh
URL=http://localhost:8000 venv/bin/ocdsadditions buildsite --url=$URL out
```

## Running

```sh
python -m http.server --directory=out
```

## Deploying

All pushes to the `main` branch will trigger a re-deploy of the site using the
standard gitlab CI/CD process.

## Tasks

For reference, the tenders have specific OCIDs that our data refers to. They are
listed here:

- Find A Tender: ocds-h6vhtk-02c615

### Creating an empty release

Designate a new release ID e.g. `rel-id-1`.

```sh
ocdsadditions addemptyrelease ocds-h6vhtk-02c615 <release-id>
```

Edit the `package.json` file with ???

### Export a spreadsheet

Choose a release that's been previously created. Parse the ID from the filename
by looking at the pattern in the directory. It will likely be `rel-id-X`.

At the root of this repo, run:

```sh
ocdsadditions createreleasespreadsheet ocds-h6vhtk-02c615 <release-id> output.xlsx
```

TODO...
